Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
Actually not. We think that we have completed the whole requirements
* Is there anything that you did that you feel might be unclear? Explain it here.
There seems to be something wrong with db.settings senetence in the createActivity. Due to the sentence, there is very small probability of breaking up 
when the user finish editing the email and password and click the create button. I have tried the app many times, and on most situations the app 
can function normally. But on very rare situation the app will break up when the user clicks create. We just cannot figure out why.


A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

Creative Portion:
  We've implemented several extra functionalities:
       
	    1. Sign in as guest:
		   * The users can sign in as guests to try the game out. The record of wins and losses when being a guest will not be kept.
		   * As we were play-testing with our friends, some of them refuse to create a new account since apparently 
		     account creation is "too much work". Therefore now they can sign in and feel the game before deciding whether or not they want to 
			 create an account.
		   * We add a button in the sign-in layout to allow the user to play the game as a guest. We add a guest document in the database to make the game
		     function appropriately. So the guest-model is actually to use the guest account we prepare for the user. But the guest can neither bid money 
			 nor keep the game records.
		   
		2. Money
		   * Each account now has a certain amount of money. Before each game, they can enter the amount of money they want to bet on this game.
		     If they win the game they win that amount of money, and if they lose they lose it. They can choose to double the bet after seeing their cards.
		   * Blackjack is a casino game after all.
		   * We add a edittext in the layout to let the user input their bidding money. If they choose not to input the bidding money, the default bid money
		     is 0. We also use a if sentence to tell whether the input has exceeded the users' money. If the input value is bigger, the game cannot start
			 and there will be a message to tell the user. After the game starts, there is a "Double" button for user to double their bidding money. Every
			 time they bid the money, there will be a message to tell the user that the money has been doubled and what the value of the doubled money. If 
			 the doubled money exceeded the money the user holds, it will not be successful. From the database perspective, we add a field in each document
			 to record the money. We give 1000 to user at first time. The user's money will be updated accordingly.And the user's money will be 
			 displayed when they sign in in a textview.
		
		3. How to play
		   * After the start of a game, the user can tab the "rules" button to see how to play the game.
		   * We found that it might be hard for the users to figure out how to "hit" and "stand", so we added a little hints for them.
		   * We add a new layout xml file to hold the rules that we want the user to know. And after each game starts, there is a button in the left. If user
		     clicks the button, we use a dialog method to display the rules. In the main activity, we set the clicklistener to button and use the
			 displayDialog function to complete the functionality.
13/15 - Creative Portion - Difficult to see money amount when betting, in between rounds
108/110